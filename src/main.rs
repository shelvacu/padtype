use std::ops::Index;

mod octant;
mod map;

use octant::OctantSection;
use octant::Polar;

pub fn modulus(a: f64, b: f64) -> f64 {
    ((a % b) + b) % b
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct LR<T> {
    pub l: T,
    pub r: T,
}

impl<T> LR<T> {
    pub fn join<U>(self, other: LR<U>) -> LR<(T, U)> {
        LR {
            l: (self.l, other.l),
            r: (self.r, other.r),
        }
    }

    pub fn map<U, F: FnMut(T) -> U>(self, mut f: F) -> LR<U> {
        LR {
            l: f(self.l),
            r: f(self.r),
        }
    }

    pub fn flip(self) -> Self {
        Self {
            l: self.r,
            r: self.l,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct XY {
    pub x: i16,
    pub y: i16,
}

impl core::ops::Sub<XY> for XY {
    type Output = Self;

    fn sub(self, rhs: XY) -> Self::Output {
        Self{
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl XY {
    pub fn x_f64(self) -> f64 {
        self.x as f64 / 32767.0
    }

    pub fn y_f64(self) -> f64 {
        self.y as f64 / 32767.0
    }

    pub fn to_polar(self) -> Polar {
        octant::xy_to_vel_cir(self.x_f64(), self.y_f64())
    }

    pub fn octant(self) -> Option<octant::OctantSection> {
        octant::polar_to_octant(self.to_polar())
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct Buttons {
    n: bool,
    e: bool,
    s: bool,
    w: bool,
}

impl Buttons {
    pub fn any(self) -> bool {
        self.n || self.e || self.s || self.w
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct MyState {
    pub sticks: LR<XY>,
    pub pads: LR<XY>,
    pub pad_strength: LR<u16>,
    pub pad_touch: LR<bool>,
    pub buttons: LR<Buttons>,
    pub l1: bool,
    pub l2: bool,
    pub l3: bool,
    pub l4: bool,
    pub r1: bool,
    pub r2: bool,
    pub r3: bool,
    pub r4: bool,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct ComputedState {
    pub plain_state: MyState,
    pub effective_octant: LR<Option<OctantSection>>,
}

impl ComputedState {
    pub fn from_plain(plain_state: MyState, lowp: LR<Option<OctantSection>>) -> Self {
        Self {
            plain_state,
            effective_octant: lowp.join(plain_state.sticks).map(|(lowp, stick)| stick.octant().or(lowp))
        }
    }

    pub fn get_primary_half(self) -> ComputedHalfState {
        ComputedHalfState { octant: self.effective_octant.l, buttons: self.plain_state.buttons.r }
    }

    pub fn get_secondary_half(self) -> ComputedHalfState {
        ComputedHalfState { octant: self.effective_octant.r, buttons: self.plain_state.buttons.l }
    }

    pub fn get_halves(self) -> LR<ComputedHalfState> {
        LR{
            l: self.get_secondary_half(),
            r: self.get_primary_half(),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct ComputedHalfState {
    pub octant: Option<OctantSection>,
    pub buttons: Buttons,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct HalfState {
    pub stick: XY,
    pub buttons: Buttons,
}

impl MyState {
    pub fn get_primary_half(self) -> HalfState {
        HalfState { stick: self.sticks.l, buttons: self.buttons.r }
    }

    pub fn get_secondary_half(self) -> HalfState {
        HalfState { stick: self.sticks.r, buttons: self.buttons.l }
    }

    pub fn get_halves(self) -> LR<HalfState> {
        LR{l: self.get_secondary_half(), r: self.get_primary_half()}
    }
}


#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub struct Transition<T> {
    pub prev: T,
    pub curr: T,
}

impl<T> Transition<T> {
    pub fn push_new(&mut self, new: T) {
        // new         self.curr
        //  |              |
        //  V              V
        // self.curr   self.prev
        self.prev = core::mem::replace(&mut self.curr, new);
    }

    pub fn change<F: FnMut(&T) -> bool>(&self, mut f: F) -> Option<bool> {
        let before = f(&self.prev);
        let now = f(&self.curr);
        match (before, now) {
            (false, true)  => Some(true),
            (true, false)  => Some(false),
            (true, true)   => None,
            (false, false) => None,
        }
    }

    pub fn map<U, F: FnMut(T) -> U>(self, mut f: F) -> Transition<U> {
        Transition { prev: f(self.prev), curr: f(self.curr) }
    }

    pub fn both<F: Fn(&T) -> bool>(&self, f: F) -> bool {
        f(&self.prev) && f(&self.curr)
    }

    pub fn as_ref(&self) -> Transition<&T> {
        Transition { prev: &self.prev, curr: &self.curr }
    }
}

impl<T: Eq> Transition<T> {
    pub fn changed(&self) -> bool {
        self.prev != self.curr
    }
}

impl<T: core::ops::Sub<T>> Transition<T> {
    pub fn diff(self) -> <T as core::ops::Sub<T>>::Output {
        self.curr - self.prev
    }
}

impl Index<usize> for Buttons {
    type Output = bool;

    fn index(&self, i: usize) -> &bool {
        match i {
            0 => &self.n,
            1 => &self.e,
            2 => &self.s,
            3 => &self.w,
            _ => panic!("{i} out of range to index Buttons, expected 0..4"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Report {
    InputData                  = 0x09,
    SetMappings                = 0x80,
    ClearMappings              = 0x81,
    GetMappings                = 0x82,
    GetAttrib                  = 0x83,
    GetAttribLabel             = 0x84,
    DefaultMappings            = 0x85,
    FactoryReset               = 0x86,
    WriteRegister              = 0x87,
    ClearRegister              = 0x88,
    ReadRegister               = 0x89,
    GetRegisterLabel           = 0x8a,
    GetRegisterMax             = 0x8b,
    GetRegisterDefault         = 0x8c,
    SetMode                    = 0x8d,
    DefaultMouse               = 0x8e,
    ForceFeedback              = 0x8f,
    RequestCommStatus          = 0xb4,
    GetSerial                  = 0xae,
    HapticPulse                = 0xea,
}

const HELP_DOC:&'static str = r#"
padtype Copyright (C) Shelvacu 2024
This program comes with ABSOLUTELY NO WARRANTY
This programs is free software, and you are welcome to redistribute it under the terms of the GNU General Public License v3

usage: padtype

padtype does not accept any arguments
"#;

fn main() {
    if let Some(_) = std::env::args_os().skip(1).next() {
        eprintln!("{}", HELP_DOC);
        std::process::exit(5);
    }

    let api = hidapi::HidApi::new().unwrap();
    let mut found = vec![];
    let mut tries = 0;
    loop {
        eprintln!("Searching for steamdeckcontrollers...");
        for dev in api.device_list() {
            if dev.vendor_id() == 0x28de && dev.product_id() == 0x1205 && dev.interface_number() == 2 {
                found.push(dev);
            }
        }

        if found.len() > 1 {
            panic!("Too many controllers! Which one...");
        }

        if found.len() == 1 {
            break;
        }

        if tries > 10 {
            panic!("Giving up, no steamdeckcontrollers found");
        }

        //no controller found
        eprintln!("No steamdeckcontrollers found! Retrying...");
        std::thread::sleep(Duration::from_millis(500));
        tries += 1;
    }

    let dev = found.into_iter().next().unwrap().open_device(&api).expect("Could not open /dev/hidraw* device");
    dbg!(&dev);

    const USE_BUILTIN_MOUSE:bool = true;

    if USE_BUILTIN_MOUSE {
        // Enable built-in mouse emulation. steamdeckcontroller firmware does a pretty good job
        // Experimentation shows
        //             0 => emulate mouse with haptics
        // anything else => do nothing
        // X/Y values and such are always sent regardless
        write_register(&dev, Register::RpadMode, 0).unwrap();
    } else {
        disable_lizard_trackpad(&dev).unwrap();
    }

    let mut keyset = AttributeSet::new();
    for i in 1..254 {
        keyset.insert(Key(i));
    }

    let mut fake_kb = VirtualDeviceBuilder::new().expect("Could not open /dev/uinput")
        .name("padtype virtual keyboard")
        .with_keys(&keyset).unwrap()
        .build().unwrap();

    let mut axisset = AttributeSet::new();
    axisset.insert(RelativeAxisType::REL_X);
    axisset.insert(RelativeAxisType::REL_Y);
    axisset.insert(RelativeAxisType::REL_WHEEL);
    axisset.insert(RelativeAxisType::REL_HWHEEL);
    axisset.insert(RelativeAxisType::REL_WHEEL_HI_RES);
    axisset.insert(RelativeAxisType::REL_HWHEEL_HI_RES);
    let mut keyset = AttributeSet::new();
    keyset.insert(Key::BTN_LEFT);
    keyset.insert(Key::BTN_RIGHT);
    let mut fake_mouse = VirtualDeviceBuilder::new().unwrap()
        .name("padtype virtual mouse")
        .with_relative_axes(&axisset).unwrap()
        .with_keys(&keyset).unwrap()
        .build().unwrap();

    let mut write_buf = [0u8; 65];
    write_buf[0] = Report::ClearMappings as u8;
    use std::time::{Instant, Duration};
    dev.write(write_buf.as_slice()).unwrap();
    let mut last_clear_mappings = Instant::now();

    let mut megastate:Transition<ComputedState> = Default::default();

    let mut last_r_pad_coords:Option<XY> = None;
    let mut last_octant_with_pressed:LR<Option<OctantSection>> = Default::default();

    let mut input_events = vec![];
    let mut mouse_events = vec![];

    let mut buf = [0u8; 128];
    loop {
        let mut any_key_down = false;
        let len = dev.read(&mut buf).unwrap();
        let inp = parse_input_report(&buf[0..len]);

        if inp.get_r_pad_touch() {
            let now_r_pad_coords = inp.pads().r;
            if let Some(coords) = last_r_pad_coords {
                let rel_x = now_r_pad_coords.x - coords.x;
                let rel_y = now_r_pad_coords.y - coords.y;
                if !USE_BUILTIN_MOUSE {
                    fake_mouse.emit(&[
                        InputEvent::new(
                            EventType::RELATIVE,
                            RelativeAxisType::REL_X.0,
                            (rel_x/150).into(),
                        ),
                        InputEvent::new(
                            EventType::RELATIVE,
                            RelativeAxisType::REL_Y.0,
                            (-rel_y/150).into(),
                        ),
                    ]).unwrap();
                }
            }
            last_r_pad_coords = Some(now_r_pad_coords);
        } else {
            last_r_pad_coords = None;
        }

        megastate.push_new(ComputedState::from_plain(inp.state(), last_octant_with_pressed));

        if megastate.changed() {
            last_octant_with_pressed = megastate.curr.get_halves().flip().map(|halv| {
                if halv.buttons.any() { halv.octant } else { None }
            });
    
            let state = megastate.map(|m| m.plain_state);

            if state.both(|s| s.pad_touch.l) {
                let radial = state.map(|s| s.pads.l.to_polar().dir);
                let diff_a = modulus((radial.curr - radial.prev), 1.0);
                let diff_b = modulus((radial.prev - radial.curr), 1.0);
                let diff = if diff_a < diff_b { diff_a } else { -diff_b };
                // let diff = state.curr.pads.l - state.prev.pads.l;
                let scroll_ticks = diff * 1000.0;
                mouse_events.push(
                    InputEvent::new(
                        EventType::RELATIVE,
                        RelativeAxisType::REL_WHEEL_HI_RES.0,
                        scroll_ticks as i32,
                    )
                );
                dbg!(&mouse_events);
            }
            if let Some(pressed) = state.change(|s| s.r2) {
                any_key_down = any_key_down || pressed;
                input_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::KEY_LEFTSHIFT.0,
                    pressed as i32,
                ));
            }
            if let Some(pressed) = state.change(|s| s.r1) {
                any_key_down = any_key_down || pressed;
                input_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::KEY_LEFTMETA.0,
                    pressed as i32,
                ));
            }
            if let Some(pressed) = state.change(|s| s.l2) {
                any_key_down = any_key_down || pressed;
                input_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::KEY_LEFTCTRL.0,
                    pressed as i32,
                ));
            }
            if let Some(pressed) = state.change(|s| s.l1) {
                any_key_down = any_key_down || pressed;
                input_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::KEY_LEFTALT.0,
                    pressed as i32,
                ));
            }
            if let Some(pressed) = state.change(|s| s.r4) {
                any_key_down = any_key_down || pressed;
                mouse_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::BTN_LEFT.0,
                    pressed as i32,
                ));
            }
            if let Some(pressed) = state.change(|s| s.l4) {
                any_key_down = any_key_down || pressed;
                mouse_events.push(InputEvent::new(
                    EventType::KEY,
                    evdev::Key::BTN_RIGHT.0,
                    pressed as i32,
                ));
            }
        }

        for left in [false, true] {
            for octant in [
                OctantSection::Center,
                OctantSection::Octant(0),
                OctantSection::Octant(1),
                OctantSection::Octant(2),
                OctantSection::Octant(3),
                OctantSection::Octant(4),
                OctantSection::Octant(5),
                OctantSection::Octant(6),
                OctantSection::Octant(7),
            ] {
                let m = if left { map::LEFT_STICK_KEYS } else { map::RIGHT_STICK_KEYS };
                let s = if left { megastate.map(|s| s.get_primary_half()) } else { megastate.map(|s| s.get_secondary_half()) };
                let actionset = match octant {
                    OctantSection::Center => &m[0..4],
                    OctantSection::Octant(i) => &m[((i as usize)+1)*4..],
                };

                #[allow(clippy::needless_range_loop)]
                for i in 0..4 {
                    let change = s.change(|s| s.octant == Some(octant) && s.buttons[i]);
                    if let Some(pressed) = change {
                        any_key_down = any_key_down || pressed;
                        input_events.push(InputEvent::new(
                            EventType::KEY,
                            actionset[i].0,
                            pressed as i32,
                        ));
                    }
                }
            }
        }

        if !input_events.is_empty() {
            fake_kb.emit(&input_events).unwrap();
        }

        if !mouse_events.is_empty() {
            fake_mouse.emit(&mouse_events).unwrap();
        }

        input_events.clear();
        mouse_events.clear();

        if any_key_down {
            send_ff(&dev, ForceFeedback {
                trackpad: TrackPad::Both,
                pulse_high_us: 0x7fff,
                pulse_low_us: 0x7fff,
                repeat_count: 4,
            }).unwrap();
        }

        if last_clear_mappings.elapsed() > Duration::from_secs(1) {
            dev.write(write_buf.as_slice()).unwrap();
            last_clear_mappings = Instant::now();
        }
    }
    
}

use arrayref::array_ref;
use evdev::{uinput::VirtualDeviceBuilder, AttributeSet, InputEvent, EventType, Key, RelativeAxisType};
use hidapi::{HidDevice, HidError};
use packed_bools::PackedBooleans;

#[derive(Debug, Copy, Clone, PackedBooleans, PartialEq, Eq, Hash)]
pub struct InputReport {
    pub unk_header: [u8; 3],
    pub size: u8,
    pub frame: u32,
    #[pack_bools(r2, l2, r1, l1, y, b, x, a)]
    pub b08: u8,
    #[pack_bools(n, e, w, s, options, steam, menu, l5)]
    pub b09: u8,
    #[pack_bools(r5, l_pad_press, r_pad_press, l_pad_touch, r_pad_touch, _unk3, l3, _unk4)]
    pub b10: u8,
    #[pack_bools(_unk5, _unk6, r3, _unk7, _unk8, _unk9, _unk10, _unk11)]
    pub b11: u8,
    pub b12: u8,
    #[pack_bools(_unk20, l4, r4, _unk21, _unk22, _unk23, l_stick_touch, r_stick_touch)]
    pub b13: u8,
    #[pack_bools(_unk24, _unk25, quick_access, _unk26, _unk27, _unk28, _unk29, _unk30)]
    pub b14: u8,
    pub b15: u8,
    pub l_pad_x: i16,
    pub l_pad_y: i16,
    pub r_pad_x: i16,
    pub r_pad_y: i16,

    pub accel_x: i16,
    pub accel_y: i16,
    pub accel_z: i16,

    pub pitch: i16,
    pub yaw:   i16,
    pub roll:  i16,

    pub _unk_gyro: [u8; 8],

    pub l_trig: u16,
    pub r_trig: u16,

    pub l_stick_x: i16,
    pub l_stick_y: i16,
    pub r_stick_x: i16,
    pub r_stick_y: i16,

    pub l_pad_force: u16,
    pub r_pad_force: u16,

    pub l_stick_force: u16,
    pub r_stick_force: u16,
}

impl InputReport {
    pub fn quads(self) -> LR<Buttons> {
        LR{
            l: Buttons{
                n: self.get_n(),
                e: self.get_e(),
                s: self.get_s(),
                w: self.get_w(),
            },
            r: Buttons{
                n: self.get_y(),
                e: self.get_b(),
                s: self.get_a(),
                w: self.get_x(),
            }
        }
    }

    pub fn sticks(self) -> LR<XY> {
        LR{
            l: XY{
                x: self.l_stick_x,
                y: self.l_stick_y,
            },
            r: XY{
                x: self.r_stick_x,
                y: self.r_stick_y,
            },
        }
    }

    pub fn pads(self) -> LR<XY> {
        LR{
            l: XY{
                x: self.l_pad_x,
                y: self.l_pad_y,
            },
            r: XY{
                x: self.r_pad_x,
                y: self.r_pad_y,
            },
        }
    }

    pub fn state(self) -> MyState {
        MyState {
            sticks: self.sticks(),
            pads: self.pads(),
            pad_strength: LR {
                l: self.l_pad_force,
                r: self.r_pad_force,
            },
            pad_touch: LR {
                l: self.get_l_pad_touch(),
                r: self.get_r_pad_touch(),
            },
            buttons: self.quads(),
            l1: self.get_l1(),
            l2: self.get_l2(),
            l3: self.get_l3(),
            l4: self.get_l4(),
            r1: self.get_r1(),
            r2: self.get_r2(),
            r3: self.get_r3(),
            r4: self.get_r4(),
        }
    }
}

fn parse_input_report(r: &[u8]) -> InputReport {
    if r.len() != 64 {
        panic!("bad size");
    }

    if r[0..3] != [0x01, 0x00, 0x09] {
        panic!("unrecognized report type");
    }

    macro_rules! bytes_num {
        ($n:ident, $a:ident, $from:literal .. $to:literal) => {
            <$n>::from_le_bytes(*array_ref![$a, $from, $to-$from])
        }
    }

    InputReport { 
        unk_header: [r[0], r[1], r[2]], 
        size: r[3],
        frame: bytes_num!(u32, r, 4..8),
        b08: r[8],
        b09: r[9],
        b10: r[10],
        b11: r[11],
        b12: r[12],
        b13: r[13],
        b14: r[14],
        b15: r[15],
        l_pad_x: bytes_num!(i16, r, 16..18),
        l_pad_y: bytes_num!(i16, r, 18..20),
        r_pad_x: bytes_num!(i16, r, 20..22),
        r_pad_y: bytes_num!(i16, r, 22..24),
        accel_x: bytes_num!(i16, r, 24..26),
        accel_y: bytes_num!(i16, r, 26..28),
        accel_z: bytes_num!(i16, r, 28..30),
        pitch:   bytes_num!(i16, r, 30..32),
        yaw:     bytes_num!(i16, r, 32..34),
        roll:    bytes_num!(i16, r, 34..36),
        _unk_gyro: *array_ref![r, 36, 8],
        l_trig: bytes_num!(u16, r, 44..46),
        r_trig: bytes_num!(u16, r, 46..48),
        l_stick_x: bytes_num!(i16, r, 48..50),
        l_stick_y: bytes_num!(i16, r, 50..52),
        r_stick_x: bytes_num!(i16, r, 52..54),
        r_stick_y: bytes_num!(i16, r, 54..56),
        l_pad_force: bytes_num!(u16, r, 56..58),
        r_pad_force: bytes_num!(u16, r, 58..60),
        l_stick_force: bytes_num!(u16, r, 60..62),
        r_stick_force: bytes_num!(u16, r, 62..64),
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum TrackPad {
    Left  = 0x01,
    Right = 0x00,
    Both  = 0x02,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct ForceFeedback {
    // magic 0x8f 0x07
    pub trackpad: TrackPad,
    // SteamControllerSinger has the "magic period ratio" 495483.0
    // which is off by a factor of two because it uses that for both on and off period
    // It's close enough to 500k/1m that I'm just gonna assume these are just in microseconds
    pub pulse_high_us: u16,
    pub pulse_low_us: u16,
    pub repeat_count: u16,
    // [0x00; 55]
}

pub fn send_ff(dev: &HidDevice, ff: ForceFeedback) -> Result<(), HidError> {
    let mut data = [0u8; 64];
    data[0] = Report::ForceFeedback as u8;
    data[1] = 11;
    data[2] = ff.trackpad as u8;
    data[3] = (ff.pulse_high_us & 0xff) as u8;
    data[4] = (ff.pulse_high_us >> 8) as u8;
    data[5] = (ff.pulse_low_us & 0xff) as u8;
    data[6] = (ff.pulse_low_us >> 8) as u8;
    data[7] = (ff.repeat_count & 0xff) as u8;
    data[8] = (ff.repeat_count >> 8) as u8;

    dev.write(data.as_slice()).map(|_| ())
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Register {
    LpadMode = 0x07,
    RpadMode = 0x08,
    RpadMargin = 0x18,
    GyroMode = 0x30,
}

fn write_register(dev: &HidDevice, register: Register, value: u16) -> Result<(), HidError> {
    let mut write_buf = [0u8; 64];
    write_buf[0] = Report::WriteRegister as u8;
    write_buf[1] = 3; // length
    write_buf[2] = register as u8;
    let val_bytes = value.to_le_bytes();
    write_buf[3] = val_bytes[0];
    write_buf[4] = val_bytes[1];
    dev.write(write_buf.as_slice()).map(|_| ())
}

fn disable_lizard_trackpad(dev: &HidDevice) -> Result<(), HidError> {
    write_register(dev, Register::RpadMode, 0x07)?;
    write_register(dev, Register::RpadMargin, 0x00)?;
    write_register(dev, Register::LpadMode, 0x07)?;
    Ok(())
}
