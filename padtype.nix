{ lib
, pkg-config
, libudev-zero
, fetchFromGitHub
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "padtype";
  version = "0.1.0";

  nativeBuildInputs = [ pkg-config ];

  buildInputs = [ libudev-zero ];

  src = builtins.path { name = "padtype-src"; path = ./.; };

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  meta = with lib; {
    description = "ill put this in later";
    homepage = "https://github.com/shelvacu/padtype";
    license = licenses.gpl3;
    maintainers = [];
  };
}