This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[license](LICENSE.txt) for more details.

# Padtype

Padtype is a usermode program to allow typing with the steam deck's buttons and joysticks in linux. It uses hidapi and reverse engineering from the [open-sd](https://gitlab.com/open-sd) project and uinput to simulate a keyboard. Because it shows as a regular keyboard device it should work everywhere a USB keyboard does, including typing a full-disk-encryption password in initramfs.

Inspired by the "daisy wheel" keyboard in Steam Big Picture that was the perfect input method for typing with a gamepad and then removed (I'm still bitter), each combination of 8 directions on the joystick and each of the 4 buttons on the other side (left joystick+A,B,X,Y or right joystick+dpad) maps to a key, and L1, L2, R1, and R2 map to Alt, Ctrl, Shift, and Meta (aka Super aka Windows key aka Command) respectively.

## Interaction between steam client and padtype

It's not advisable to run the steam client and padtype at the same time. It will work, but both programs will receive the same inputs from the controls, and both programs will act on them and emulate keyboard presses or whatever, leading to duplicated input.

## Todo

* ~~Make the left trackpad scroll (right now it does nothing)~~
* ~~Fix keys sometimes double-pressing~~
* ~~Remove dependency on libudev.so (systemd) so that this can be staticly compiled~~ Done!
* Implement a mode switch to simulate a gamepad so you can play games yay

## Build with nix flakes

**If you have [Nix: the package manager](https://github.com/DeterminateSystems/nix-installer?tab=readme-ov-file#usage) installed:**

You must have [flakes enabled](https://nixos.wiki/wiki/Flakes) for these commands to work.

`nix build` in the cloned directory. It's that easy. The resulting binary will be in `result/bin/padtype` and will be statically linked.

`nix develop` will drop you in a shell with everything necessary and all the usual `cargo` commands will just work.

You can even do `sudo nix run gitlab:shelvacu/padtype` to have nix clone it for you and run it directly.

## Build (traditional linux)

On the steam deck OS, libudev headers and package-config are not present. I recommend [installing nix on the steam deck](https://github.com/DeterminateSystems/nix-installer?tab=readme-ov-file#usage) then using the steps above. You may also be able to make the system partition writable (not recommended, changes will be lost on the next update) and install the necessary packages. I haven't tried this.

On most linux systems it's as simple as [installing rust](https://rustup.rs), `pkg-config`, and some `libudev` package (part of systemd, so often already present. You may need `libudev-dev`), and then running `cargo build`.

## License

padtype Copyright (C) Shelvacu 2024

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[license](LICENSE.txt) for more details.

This project is free software licensed under the GNU General Public License v3.0. See the [license](LICENSE.txt) for full license.