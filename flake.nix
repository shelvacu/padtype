{
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  outputs = { self, nixpkgs, ... }@inputs: 
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    packages.${system} = {
      default = pkgs.pkgsStatic.callPackage ./padtype.nix {};
      padtype-systemd = pkgs.callPackage ./padtype.nix { libudev-zero = pkgs.systemdLibs; };
    };
    devShells.${system}.default = pkgs.mkShell {
      inputsFrom = [ self.packages.${system}.default ];
    };
  };
}
